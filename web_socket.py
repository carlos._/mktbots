from websocket import WebSocket
import json
from time import sleep

CC = ['trxusdt','xrpusdt','dogeusdt','batusdt']
INTERVAL = '1d'
SOCKET = f'wss://stream.binance.com:9443/ws/'
WS = WebSocket()


#def receive_data(ws,cc,interval):
def receive_data():
    s_data ={}
    for i in CC:
        WS.connect(SOCKET+f'{i}@kline_{INTERVAL}')
        #ws.send("1")
        json_mesg = json.loads(WS.recv())
        candle = json_mesg['k']
        s_data[i]={ 'symbol':i,
                    'high':candle['h'],
                    'low':candle['l'],
                    'vol':candle['v'],
                    'close':candle['c']
                   }
        sleep(0.4)
    WS.close()
    return s_data


#receive_data(WS,CC,INTERVAL)

#for i in s_data:
#    print(f'{i} -> {s_data[i]}')
