from flask import Flask, render_template
import data_in,json


app = Flask(__name__)


@app.route('/')
def m_page():
    title = 'b0-crypto'
    #cctest = data_in.get_klines()
    symbs = data_in.get_symbs()
    print(symbs)

    return render_template('index.html',
                            title=title,symbs=symbs)



@app.route('/klines/<string:k_symb>')
def kline_pairs(k_symb):
    k_symbs = data_in.get_klines(k_symb)
    return json.dumps(k_symbs)


@app.route('/pairs')
def get_pairs():
    return json.dumps(data_in.get_symbs())
