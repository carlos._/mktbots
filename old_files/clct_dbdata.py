import config_bin, sqlite3
from binance.client import Client
from datetime import datetime


client = Client(config_bin.APIK, config_bin.SECK)
conn = sqlite3.connect('./data/currency.db')
c = conn.cursor()


def get_candle_data(symb):
    """
        Function to get the candlestick data 
        # [TODO] Coletar dados dos símbolos
    """
    #l_kline = (kline_last_date(symb))
    klines = client.get_historical_klines(
                symb, 
                Client.KLINE_INTERVAL_15MINUTE, 
                "1 Jan, 2020 UTC-3"
            )
    data_trt(symb,klines)
    klines = []


def data_trt(symb,klines):
    for l in klines:
        c.execute("""INSERT INTO currency_price(currency_symbol,open_time, 
                        open,high,low,close,volume,close_time,quote_asset_volume,
                        trades,taker_base_asset_volume,taker_quote_asset_volume,
                        ignore
                    ) 
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)""", (symb,l[0],l[1],l[2],l[3],
                            l[4],l[5],l[6],l[7],l[8],l[9],l[10],l[11])
                )
    print(symb, "TERMINADO\n")


#Coleta os símbolos da table
c.execute("""SELECT id, symbol, nome FROM currency""")
s_rows = c.fetchall()


#Coleta simbolos com base na table.
for row in s_rows:
    get_candle_data(row[1])


conn.commit()
c.close()
