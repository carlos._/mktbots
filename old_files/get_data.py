import config_bin, csv
from binance.client import Client
from datetime import datetime


client = Client(config_bin.APIK, config_bin.SECK)


tst_ticks = ["ETHUSDT", "BTCUSDT"]


def tick_prc(symb):
    print(client.get_symbol_ticker(symbol=symb))


def get_candle_data(symb):
    """
        Function to get the candlestick data 
        # [TODO] Coletar dados dos futuros dos símbolos
    """
    l_kline = (kline_last_date(symb))
    klines = client.get_historical_klines(
            symb, 
            Client.KLINE_INTERVAL_15MINUTE, 
            l_kline+" UTC-3"
            )
    write_csv_data(symb,klines)

    print(symb+" data UPDATE OK!")

    return 0


def write_csv_data(symb, l_lines):
    with open("candle"+symb+".csv", 'a', newline='') as csvfile:
        file_writer = csv.writer(csvfile, delimiter=',')
        file_writer.writerows(l_lines)

        #for ln in l_lines:
        #    file_writer.writerow(ln)
        csvfile.close()


def get_kline_tstamp(l_csv):
    """ 
        Formato das klines:
        opentime, open, high, low, close
        volume, closetime, q-asset, volume, 
        n-trades, taker-buy-base-asset-volume
        taker-buy-quote-asset-volume, Ignore
    """
    tl_csv = l_csv.partition(",")[0][:-3]
    date_time = datetime.fromtimestamp(int(tl_csv))
    return date_time


def kline_last_date(symb):
    """
        Sessão teste para a função que compara a ultima
        data registrada nos csv dos símbolos
    """
    ll_read = open("candle"+symb+".csv", "r")
    line_csv = ll_read.readlines()[-1]
    ll_read.close()
    
    l_csv = get_kline_tstamp(line_csv)
    tk_time = l_csv.strftime("%d %b, %Y %I:%M %P")
    return tk_time


#Teste das funções
#for tk in tst_ticks:
#    get_candle_data(tk)
