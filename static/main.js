const interval = '1d';
let ws = new Object();
let symbs = [];
let s_streams = '';
let histKlineData = {};

//Objeto de interação com os charts tabelas.
let pairsCandleSeries = new Object();
let pairsCandleObjects = new Object();


//Função das requests para o flask
async function fetchBackData(g_path){
	const response = await fetch(g_path);
	const retData = await response.json();

	return retData;
}


async function urlMount(ini){
	let ss = [];
	const inFunc = async function(){
		ss = await fetchBackData('/pairs');
		for(let s in ss){
			s_streams+=ss[s].toLowerCase()+'@kline_'+interval+'/';
		}
		ini+=s_streams.slice(0,-1);
		return ini;
	};
	await inFunc();
	return await inFunc();
}


function lTable(str){
	var ltb = document.getElementsByName(str)[0];
	if(ltb !== undefined){
		return ltb;
	}else{
		return null;
	}
}


async function data_up(dt){
	tbr = lTable(dt.s.toString());

	if(tbr === null){
		let prnt = document.getElementById('fav_tab');
		let tbr2 = document.createElement('tr');
		let ntd2 = document.createElement('td');

		ntd2.setAttribute('id', dt.s+"-chart");
		ntd2.setAttribute('colspan', '6');
		tbr2.appendChild(ntd2);
		tbr = document.createElement('tr');

		for(i = 0; i <= 5; i++){
			var ntd = document.createElement('td');
			ntd.setAttribute('class','fav-cell');
			tbr.appendChild(ntd);
		}

		prnt.getElementsByTagName('tbody')[0].appendChild(tbr);
		prnt.getElementsByTagName('tbody')[0].appendChild(tbr2);
		tbr.setAttribute('name', dt.s);

		histKlineData = await fetchBackData('/klines/'+dt.s);
		pairsCandleObjects[dt.s] = makeCandleObjects(pairsCandleObjects[dt.s],dt.s);
		pairsCandleSeries[dt.s] = makeCandleSeries(pairsCandleObjects[dt.s],dt.s);
		mainChart(dt.s, histKlineData);
	}

	var cels = tbr.getElementsByTagName('td');
	cels[0].innerHTML = dt.s;
	cels[1].innerHTML = dt.o;
	cels[2].innerHTML = dt.h;
	cels[3].innerHTML = dt.l;
	if(parseFloat(dt.c)<parseFloat(dt.o)){
		cels[4].setAttribute('class','downNum');
	}else{
		cels[4].setAttribute('class','upNum');
	}
	cels[4].innerHTML = dt.c;
	cels[5].innerHTML = dt.v.slice(0,-6);
}


function updateCandleSeries(pairKLine){
	let objPair = pairsCandleSeries[pairKLine.s];
	//let cSeries = objPair.chart.addCandlestickSeries;

	objPair.update({
		time: pairKLine.t/1000,
		open: pairKLine.o,
		high: pairKLine.h,
		low: pairKLine.l,
		close: pairKLine.c
	})
}


function makeWS(dd){
	ws = new WebSocket(dd);
	ws.onmessage = function(event){
		let upData = JSON.parse(event.data);
		let pairKLine = upData.data.k;
		data_up(pairKLine);
		updateCandleSeries(pairKLine);
	}
}


wsURL = 'wss://stream.binance.com:9443/stream?streams=';
ss = urlMount(wsURL);

ss.then((data)=> {
	makeWS(data);
});


