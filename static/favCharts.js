function chartCreate(inObj,oKlines){
	inObj.setData(oKlines);
}


function makeCandleObjects(candleObj){
	candleObj = {
			width: 500,
			height: 240,

			layout: {
				backgroundColor: '#000000',
				textColor: 'rgba(255, 255, 255, 0.9)',
			},
			rightPriceScale: {
				scaleMargins: {
					top: 0.3,
					bottom: 0.25,
				},
			},
			grid: {
				vertLines: {
				color: '#111111',
			},
			horzLines: {
				color: '#111111',
									},
			},
			crosshair: {
				mode: LightweightCharts.CrosshairMode.Normal,
			},
			rightPriceScale: {
				borderColor: 'rgba(197, 203, 206, 0.8)',
			},
			timeScale: {
						borderColor: 'rgba(197, 203, 206, 0.8)',
			}

	}
	return candleObj;
}


function makeCandleSeries(candleObj, symb){
	var candle = LightweightCharts.createChart(document.getElementById(symb+"-chart"), candleObj);
	candleObj.chart = candle;
	var candleSeries = candle.addCandlestickSeries({
		  upColor: '#77DD77',
		  borderUpColor: '#77DD77',
		  wickUpColor: '#77DD77',
	
		  downColor: '#DD7777',
		  borderDownColor: '#DD7777',
		  wickDownColor: '#DD7777',
	});
	return candleSeries;
}
