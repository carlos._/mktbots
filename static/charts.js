//Trata os dados dos charts e envia para criação
function mainChart(k,klines){
	let outKlines = [];

	for(i in klines){
		outKlines[i] = 
				{ 
					'time': klines[i][0]/1000,
					'open': klines[i][1],
					'high': klines[i][2],
					'low': klines[i][3],
					'close': klines[i][4]
				}
	}

	chartCreate(pairsCandleSeries[k],outKlines);
	outKlines = [];
}
