from binance.client import Client
import config_bin,csv


SYMBS = []


def get_symbs():
    with open('pairs.csv',newline='') as csvfile:
        data = list(csv.reader(csvfile))
        SYMBS = data[0]
    return SYMBS


def get_klines(symb):
    client = Client(config_bin.APIK, config_bin.SECK)
    klines = {}
    klines = client.get_historical_klines(symb, Client.KLINE_INTERVAL_1DAY,"60 day ago UTC")
    print(symb)

    return klines

